<?php

namespace P11YBlocks;

class Block {
    
    public function __construct(
        protected string $name,
        protected array $options = []
    ) {}
}
